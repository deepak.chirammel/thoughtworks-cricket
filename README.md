## Pre-requisites
- JDK 1.8
- IntelliJ

## Running from IntelliJ IDE
- Right Click on the `Main.java` class and hit `Run/Debug` to run the application.
- Right Click on any of the `*Tests.java` class and hit `Run/Debug` to run tests.

## Running from Gradle Palette
- Navigate to `Tasks->application` and double click the `run` gradle task.
- Similarly `Tasks->verification->test` to run the full Unit Tests.

## Running from Command Line/Terminal
- gradlew test
- gradlew run
- gradlew run --args="2 18 0 1"

> Running application with arguments `gradlew run --args="<overs> <target> <batsmanType> <bowlerType>"` <br/>
> Batsman Types: `0 - Normal, 1 - Hitter, 2 - Defender, 3 - TailEnder` <br/>
> Bowler Types: `0 - Normal, 1 - PartTime` <br/>

> Note: For Unix based terminals (like `git bash`), navigate to the project directory and run `./gradlew <taskname>` 
