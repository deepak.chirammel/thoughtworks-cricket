package org.thoughtworks.game;

import org.thoughtworks.model.Batsman;
import org.thoughtworks.model.Bowler;

public class Match {

    private static final int MAX_BALLS_IN_AN_OVER = 6;
    private static final int MAX_RUNS_IN_AN_OVER = 36;

    private int score;
    private int overs;
    private int target;
    private Bowler bowler;
    private Batsman batsman;

    public Match(int overs, int target, Batsman batsman, Bowler bowler) {
        this.score = 0;
        this.overs = overs;
        this.target = target;
        this.batsman = batsman;
        this.bowler = bowler;
    }

    public void playMatch() {
        if (overs * MAX_RUNS_IN_AN_OVER <= target) {
            System.out.println("Unrealistic Target!!!");
            return;
        }
        int maximumBalls = MAX_BALLS_IN_AN_OVER * this.overs;
        for (int ball = 1; ball <= maximumBalls && this.score < this.target; ball++) {
            int runTaken = this.batsman.hit();
            int runGiven = this.bowler.bowl();
            System.out.println("Batsman: " + runTaken);
            System.out.println("Bowler: " + runGiven + "\n");
            if (runGiven == runTaken) {
                if (this.bowler.canGetWicket()) {
                    break;
                }
            } else if (this.batsman.isEndGameBatsman() && (runGiven % 2 == runTaken % 2) && this.bowler.canGetWicket()) {
                break;
            } else {
                this.score += runTaken;
            }
        }
    }

    public void result() {
        if (score < target) {
            System.out.println("Batsman LOST!!!");
        } else {
            System.out.println("Batsman WON!!!");
        }
    }
}
