package org.thoughtworks.enums;

public class BatsmanType {

    public static final int Normal = 0;
    public static final int Hitter = 1;
    public static final int Defender = 2;
    public static final int TailEnder = 3;
}
