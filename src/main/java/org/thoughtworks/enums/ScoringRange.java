package org.thoughtworks.enums;

public class ScoringRange {

    public static final int[] normalRange = {0, 1, 2, 3, 4, 5, 6};
    public static final int[] hitterRange = {0, 4, 6};
    public static final int[] defenderRange = {0, 1, 2, 3};
}
