package org.thoughtworks.model;

import org.thoughtworks.enums.BowlerType;
import org.thoughtworks.enums.ScoringRange;

import java.util.Random;

public class Bowler extends Player {

    private int bowlerType;

    public Bowler(String name, int bowlerType) {
        super(name);
        this.bowlerType = bowlerType;
    }

    public int bowl() {
        return ScoringRange.normalRange[new Random().nextInt(ScoringRange.normalRange.length)];
    }

    public boolean canGetWicket() {
        return this.bowlerType == BowlerType.Normal;
    }
}
