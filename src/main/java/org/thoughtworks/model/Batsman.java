package org.thoughtworks.model;

import org.thoughtworks.enums.BatsmanType;
import org.thoughtworks.enums.ScoringRange;

import java.util.Random;

public class Batsman extends Player {

    private int batsmanType;

    public Batsman(String name, int batsmanType) {
        super(name);
        this.batsmanType = batsmanType;
    }

    public boolean isEndGameBatsman() {
        return this.batsmanType == BatsmanType.TailEnder;
    }

    public int hit() {
        switch (batsmanType) {
            case BatsmanType.Normal:
            case BatsmanType.TailEnder:
                return scoredRun(ScoringRange.normalRange);
            case BatsmanType.Hitter:
                return scoredRun(ScoringRange.hitterRange);
            case BatsmanType.Defender:
                return scoredRun(ScoringRange.defenderRange);
            default:
                return 0;
        }
    }

    private int scoredRun(int[] scoreRange) {
        return scoreRange[new Random().nextInt(scoreRange.length)];
    }
}
