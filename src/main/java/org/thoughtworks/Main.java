package org.thoughtworks;

import org.thoughtworks.model.Batsman;
import org.thoughtworks.model.Bowler;
import org.thoughtworks.game.Match;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int overs = args.length > 0 ? new Integer(args[0]) : new Random().nextInt(4) + 1;
        int target = args.length > 1 ? new Integer(args[1]) : new Random().nextInt(overs * 36);
        int batsmanType = args.length > 2 ? new Integer(args[2]) : new Random().nextInt(4);
        int bowlerType = args.length > 3 ? new Integer(args[3]) : new Random().nextInt(2);
        System.out.println("Overs: " + overs);
        System.out.println("Target: " + target);
        System.out.println("BatsmanType: " + batsmanType);
        System.out.println("BowlerType: " + bowlerType + "\n");
        playGame(overs, target, batsmanType, bowlerType);
    }

    private static void playGame(int overs, int target, int batsmanType, int bowlerType) {
        Batsman batsman = new Batsman("Raidu", batsmanType);
        Bowler bowler = new Bowler("Jadeja", bowlerType);

        Match match = new Match(overs, target, batsman, bowler);
        match.playMatch();
        match.result();
    }
}
