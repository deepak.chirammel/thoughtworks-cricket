package org.thoughtworks.game;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.thoughtworks.enums.BatsmanType;
import org.thoughtworks.enums.BowlerType;
import org.thoughtworks.model.Batsman;
import org.thoughtworks.model.Bowler;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MatchTests {

    private static ByteArrayOutputStream outputStream;

    private Match createMatch(int overs, int target) {
        return new Match(overs, target,
                new Batsman("Sachin", BatsmanType.Normal),
                new Bowler("Harbhajan", BowlerType.Normal));
    }

    @BeforeClass
    public static void setUpPseudoOutputStream() {
        //Setup a mock pseudo PrintStream for getting/testing System.out stream logs
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    public void matchForValidCaseTest() {// 2 overs and 22 runs target
        outputStream.reset();
        int overs = 2;
        int target = 22;
        Match match = createMatch(overs, target);
        match.playMatch();
        Assert.assertTrue(outputStream.toString().startsWith("Batsman: "));
        Assert.assertTrue(outputStream.toString().endsWith("\n\r\n"));

        outputStream.reset();
        match.result();
        Assert.assertTrue(outputStream.toString().equals("Batsman WON!!!\r\n") || outputStream.toString().equals("Batsman LOST!!!\r\n"));
    }

    @Test
    public void matchForUnrealisticTargetCaseTest() {// 2 overs and 76 runs target
        outputStream.reset();
        int overs = 2;
        int target = 76;
        Match match = createMatch(overs, target);
        match.playMatch();
        Assert.assertTrue(outputStream.toString().equals("Unrealistic Target!!!\r\n"));

        outputStream.reset();
        match.result();
        Assert.assertTrue(outputStream.toString().equals("Batsman LOST!!!\r\n"));
    }
}
