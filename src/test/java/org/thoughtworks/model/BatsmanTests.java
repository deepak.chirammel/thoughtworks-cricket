package org.thoughtworks.model;

import org.junit.Assert;
import org.junit.Test;
import org.thoughtworks.enums.BatsmanType;

public class BatsmanTests {

    private Batsman getBatsman(String name, int batsmanType) {
        return new Batsman(name, batsmanType);
    }

    @Test
    public void normalBatsmanTypeTest() {
        String batsmanName = "Sachin";
        Batsman batsman = getBatsman(batsmanName, BatsmanType.Normal);
        Assert.assertEquals(batsmanName, batsman.getName());
        Assert.assertFalse("Normal Batsman should not be an EndGamer Batsman", batsman.isEndGameBatsman());
        int runTaken = batsman.hit();
        Assert.assertTrue("Run scored by a Normal batsman should be between 0 to 6",
                runTaken >= 0 && runTaken <= 6);
    }

    @Test
    public void hitterBatsmanTypeTest() {
        String batsmanName = "Sehwag";
        Batsman batsman = getBatsman(batsmanName, BatsmanType.Hitter);
        Assert.assertEquals(batsmanName, batsman.getName());
        Assert.assertFalse("Hitter Batsman should not be an EndGamer Batsman", batsman.isEndGameBatsman());
        int runTaken = batsman.hit();
        Assert.assertTrue("Run scored by a Hitter batsman should be 0, 4 or 6",
                runTaken == 0 || runTaken == 4 || runTaken == 6);
    }

    @Test
    public void defenderBatsmanTypeTest() {
        String batsmanName = "Dravid";
        Batsman batsman = getBatsman(batsmanName, BatsmanType.Defender);
        Assert.assertEquals(batsmanName, batsman.getName());
        Assert.assertFalse("Defender Batsman should not be an EndGamer Batsman", batsman.isEndGameBatsman());
        int runTaken = batsman.hit();
        Assert.assertTrue("Run scored by a Defender batsman should be between 0 to 3",
                runTaken >= 0 && runTaken <= 3);
    }

    @Test
    public void tailEnderBatsmanTypeTest() {
        String batsmanName = "Dhoni";
        Batsman batsman = getBatsman(batsmanName, BatsmanType.TailEnder);
        Assert.assertEquals(batsmanName, batsman.getName());
        Assert.assertTrue("TailEnder Batsman should be an EndGamer Batsman", batsman.isEndGameBatsman());
        int runTaken = batsman.hit();
        Assert.assertTrue("Run scored by a TailEnder batsman should be between 0 to 6",
                runTaken >= 0 && runTaken <= 6);
    }
}
