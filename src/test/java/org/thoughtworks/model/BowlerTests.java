package org.thoughtworks.model;

import org.junit.Assert;
import org.junit.Test;
import org.thoughtworks.enums.BowlerType;

public class BowlerTests {

    private Bowler getBowler(String name, int bowlerType) {
        return new Bowler(name, bowlerType);
    }

    @Test
    public void normalBowlerTypeTest() {
        String name = "Ramesh";
        Bowler bowler = getBowler(name, BowlerType.Normal);
        Assert.assertEquals(name, bowler.getName());
        Assert.assertTrue("Normal Bowler can get wicket", bowler.canGetWicket());
        int runGiven = bowler.bowl();
        Assert.assertTrue("Run given should be between 0 to 6",
                runGiven >= 0 && runGiven <= 6);
    }

    @Test
    public void partTimeBowlerTypeTest() {
        String name = "Suresh";
        Bowler bowler = getBowler(name, BowlerType.PartTime);
        Assert.assertEquals(name, bowler.getName());
        Assert.assertFalse("PartTime bowler cannot get wicket", bowler.canGetWicket());
        int runGiven = bowler.bowl();
        Assert.assertTrue("Run given should be between 0 to 6",
                runGiven >= 0 && runGiven <= 6);
    }
}
