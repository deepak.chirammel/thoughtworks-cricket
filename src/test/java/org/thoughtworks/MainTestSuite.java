package org.thoughtworks;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.thoughtworks.enums.BatsmanType;
import org.thoughtworks.enums.BowlerType;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTestSuite {

    private static ByteArrayOutputStream outputStream;

    @BeforeClass
    public static void setUpPseudoOutputStream() {
        //Setup a mock pseudo PrintStream for getting/testing System.out stream logs
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    public void mainTest() {
        // In case of empty arguments - no values provided in the command line
        outputStream.reset();
        Main.main(new String[]{});
        Assert.assertTrue(outputStream.toString().startsWith("Overs: "));
        Assert.assertTrue(outputStream.toString().endsWith("Batsman WON!!!\r\n")
                || outputStream.toString().endsWith("Batsman LOST!!!\r\n"));

        // In case of 1 argument - `overs` value provided in command line
        outputStream.reset();
        Main.main(new String[]{"2"});
        Assert.assertTrue(outputStream.toString().startsWith("Overs: 2\r\nTarget: "));
        Assert.assertTrue(outputStream.toString().endsWith("Batsman WON!!!\r\n")
                || outputStream.toString().endsWith("Batsman LOST!!!\r\n"));

        // In case of 2 arguments - `overs` and `target` values provided in command line
        outputStream.reset();
        Main.main(new String[]{"3", "25"});
        Assert.assertTrue(outputStream.toString().startsWith("Overs: 3\r\nTarget: 25\r\nBatsmanType: "));
        Assert.assertTrue(outputStream.toString().endsWith("Batsman WON!!!\r\n")
                || outputStream.toString().endsWith("Batsman LOST!!!\r\n"));

        // In case of 3 arguments - `overs`, `target` and `BatsmanType` values provided in command line
        outputStream.reset();
        Main.main(new String[]{"1", "15", ((Integer)BatsmanType.Hitter).toString()});
        Assert.assertTrue(outputStream.toString().startsWith("Overs: 1\r\nTarget: 15\r\nBatsmanType: 1\r\nBowlerType: "));
        Assert.assertTrue(outputStream.toString().endsWith("Batsman WON!!!\r\n")
                || outputStream.toString().endsWith("Batsman LOST!!!\r\n"));

        // In case of 4 arguments - `overs`, `target`, `BatsmanType`, and `BowlerType` values provided in command line
        outputStream.reset();
        Main.main(new String[]{"1", "7", ((Integer)BatsmanType.Defender).toString(), ((Integer) BowlerType.PartTime).toString()});
        Assert.assertTrue(outputStream.toString().startsWith("Overs: 1\r\nTarget: 7\r\nBatsmanType: 2\r\nBowlerType: 1\n\r\n"));
        Assert.assertTrue(outputStream.toString().endsWith("Batsman WON!!!\r\n")
                || outputStream.toString().endsWith("Batsman LOST!!!\r\n"));
    }
}
